﻿; (function ($) {
    $.fn.kaslide = function (opts) {
        var wrap = $(this);
        var w = wrap.width(), h = wrap.height();
        var walls = new Array();
        var thumbs = new Array();
        var init = false;

        $(wrap).append('<div class="featured_walls"></div>');
        $(wrap).append('<ul class="pagination"></ul>');

        $('> div', '.featured_src').each(function () {
            if ($(this).attr('id') == 'featurama') {
                walls.push($(this).attr('data-src') + '[' + $(this).attr('id') + ']');
                thumbs.push($(this).attr('data-thumb') + '[' + $(this).attr('id') + ']');
            }
            else {
                walls.push($(this).attr('data-src'));
                thumbs.push($(this).attr('data-thumb'));
            }
        });

        var featuredAmount = walls.length;
        for (var i = 0; i < featuredAmount; i++) {
            $('ul.pagination').append('<li class= ' + i + '_pag' + '></li>');
        }

        for (var featuredId = 0; featuredId < walls.length; featuredId++) {
           var src = "/" + walls[featuredId].substring(0, walls[featuredId].indexOf('s', walls[featuredId].indexOf('/') + 1)) +
                '/' + walls[featuredId].substring((walls[featuredId].indexOf('-') + 1), walls[featuredId].indexOf('.'));//ie: /wallpaper/13526
            $('.featured_walls').append('<div class = ' + featuredId + '_featured_slide' + '><a class='+ featuredId + '_featured_link href='+src+'></a></div>');
            $('.' + featuredId + '_featured_slide').addClass('featured_slide').hide();
            if (walls[featuredId].indexOf('featurama') >= 0) {
                $('.' + featuredId + '_featured_slide').attr('id', 'featurama');
            }
        }

        loadFeatured();

        function loadFeatured(featuredId, curPagId) {

            if (featuredId == undefined) {
                featuredId = parseInt($('#featurama', '.featured_walls').attr('class'));
                $('li.' + featuredId + '_pag').addClass('focus');

                var el = new Image();
                var src = walls[featuredId];

                if (src.indexOf('featurama') >= 0) {
                    src = src.substring(0, src.indexOf('['));
                }

                if (!$('img.loadedFt', '.' + featuredId + '_featured_slide').length) {
                    $('.' + featuredId + '_featured_link').append($(el).attr('class', 'loadedFt').hide());
                }

                el.onload = function () {
                    var nH, nW;
                    nH = el.naturalHeight;
                    nW = el.naturalWidth;
                    $(el).attr('height', nH);
                    $(el).attr('width', nW);
                    resizeFeatured(function () {
                        w = wrap.width();
                        h = wrap.height();

                        var bw = Math.floor(w / h * 20),
                            bh = Math.floor(w / h * 20);
                        var fw = $('img.loadedFt', '.' + featuredId + '_featured_slide').css('width'),
                            fh = $('img.loadedFt', '.' + featuredId + '_featured_slide').css('height');
                        var rows = h / bh;
                        var cols = w / bw;
                        var mTop = Math.floor($('img.loadedFt', '.' + featuredId + '_featured_slide').css('margin-top'));
                        $('.' + featuredId + '_featured_slide').addClass('currentFt');
                        $('.' + featuredId + '_featured_slide').show();
                        $('img', '.' + featuredId + '_featured_slide').fadeIn(400);
                    });
                }
                el.src = src;
            }
            else {
                //apply blockbuster so that only 1 animation can run at a time.
                wrap.addClass('blockbuster');
                var el = new Image();
                var src = walls[featuredId];

                if (src.indexOf('featurama') >= 0) {
                    src = src.substring(0, src.indexOf('['));
                }

                if (!$('img.loadedFt', '.' + featuredId + '_featured_slide').length) {
                    $('.' + featuredId + '_featured_link').append($(el).attr('class', 'loadedFt').hide());
                }

                el.onload = function () {
                    var nH, nW;
                    nH = el.naturalHeight;
                    nW = el.naturalWidth;
                    $(el).attr('height', nH);
                    $(el).attr('width', nW);
                    resizeFeatured(function () {
                        w = wrap.width();
                        h = wrap.height();

                        //var bw = Math.floor(w / h * 20), bh = Math.floor(w / h * 20);
                        var bw = 60, bh = 60;
                        var fw = $('img.loadedFt', '.' + featuredId + '_featured_slide').css('width'), fh = $('img.loadedFt', '.' + featuredId + '_featured_slide').css('height');
                        var rows = h / bh;
                        var cols = w / bw;
                        var mTop = Math.floor($('img.loadedFt', '.' + featuredId + '_featured_slide').css('margin-top'));

                        var blockcontainer = $(document.createElement('div'));

                        blockcontainer.addClass('blocks');
                        blockcontainer.css({
                            'position': 'absolute',
                            'width': w,
                            'height': h,
                            'overflow': 'hidden',
                            'top': 0
                        });

                        for (var r = 0; r < rows; r++) {
                            for (var c = 0; c < cols; c++) {
                                var block = $(document.createElement('div'));
                                block.css({
                                    'position': 'absolute',
                                    'width': bw,
                                    'height': bh,
                                    'top': bh * r,
                                    'left': bw * c,
                                    'overflow': 'hidden',
                                    'z-index': '950',
                                    'display': 'none'
                                });
                                block.addClass('block');
                                var featuredClone = $('img.loadedFt', '.' + featuredId + '_featured_slide').clone();
                                featuredClone.css({
                                    'position': 'relative',
                                    'width': fw,
                                    'height': fh,
                                    'margin-top': mTop,
                                    'top': -parseInt($(block).css('top')) + 'px',
                                    'left': -parseInt($(block).css('left')) + 'px',
                                    'visibility': 'visible',
                                    'display': 'block'
                                });
                                $(block).append(featuredClone);
                                blockcontainer.append(block);
                                $(wrap).append(blockcontainer);
                            }
                        }
                        //animate the next wallpaper
                        var ai = setInterval(function () {
                            //block animation
                            if ($(blockcontainer).children('div.block').not(':visible').length > 0) {
                                var blockArr = $('.blocks').children('div.block').not(':visible');
                                blockArr.eq(Math.floor(Math.random() * blockArr.length)).fadeIn(100);
                            }
                            //after the animation is done, initialize the corresponding featured wallpaper data
                            else if ($('.block:animated').length == 0) {
                                clearInterval(ai);
                                $('img.loadedFt', '.' + featuredId + '_featured_slide').show();
                                $('.' + featuredId + '_featured_slide').show();
                                $('.' + curPagId + '_featured_slide').hide();
                                $('img.loadedFt', '.' + curPagId + '_featured_slide').hide();
                                $('.' + featuredId + '_featured_slide').addClass('currentFt');
                                blockcontainer.remove();
                                wrap.removeClass('blockbuster');
                            }
                        }, 25);
                    });
                }
                el.src = src;
            }
        }

        //load the corresponding featured wallpaper on pagination select
        $('ul.pagination li').bind('click', function () {
            if (!wrap.hasClass('blockbuster')) {
                var pagId = parseInt($(this).attr('class'));
                var curPagId = parseInt($('.currentFt').attr('class'));
                
                if (pagId != curPagId) {
                    $(this).addClass('focus');
                    $('.' + curPagId + '_featured_slide').removeClass('currentFt');
                    $('li.' + curPagId + '_pag').removeClass('focus');
                    loadFeatured(pagId, curPagId);
                }
            }
        });

        //show thumbnail on pagination hover
        $('ul.pagination li').hover(function () {
            var thumbId = parseInt($(this).attr('class'))
            var pos = $(this).offset();
            var src = thumbs[thumbId];
            //var src = walls[thumbId];
            if (src.indexOf('featurama') >= 0) {
                src = src.substring(0, src.indexOf('['));
            }
            $(wrap).append('<div class="featured_thumb" style="left:' + (pos.left - 45) + 'px; top:205px;"><img src="' + src + '" /></div>');
            $('.featured_thumb').animate({
                'opacity': 1,
                'top': 180
            }, 200);
        }, function () {
            $('.featured_thumb').remove();
        });

        //resize feauted wallpapers on resize
        $(window).bind('resize', function () {
            resizeFeatured();
        });

        //resize and center featured wallpapers to a proportional size
        function resizeFeatured(callback) {
            if (callback != undefined) {
                callback();
            }
            w = wrap.width();
            $('img.loadedFt').each(function () {
                var t = $(this),
                tH = t.attr('height');
                tW = t.attr('width');
                if ((tW / tH) < (w / h)) {
                    var r = w / tW;
                    var marginTop = Math.floor((h - (tH * r)) / 2) + 'px';
                    t.css({
                        'height': tH * r,
                        'width': w,
                        'margin-left': 0,
                        'margin-top': marginTop,
                        'position': 'absolute'
                    });
                } else{
                    var r = w / tW;
                    t.css({
                        'height': 300,
                        'width': 480,
                        'margin-left': 0,
                        'margin-top': 0,
                        'position': 'absolute'
                    });
                }
            });
        }
    }

})(jQuery);