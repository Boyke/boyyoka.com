(function ($) {
    $.fn.tagCollage = function (opts) {

        //hover effect for tag
        $('#tag_wrap').on({
            mouseenter: function () {
                $('.tag_name').remove();
                $(this).append('<div class="tag_name">' + $('img', this).data('data-tag') + '</div>');
                $('.tag_name').animate({
                    'opacity': 1,
                    'bottom': 0
                }, 200);
            }, mouseleave: function () {
                $('.tag_name').animate({
                    'opacity': 1,
                    'bottom': -25
                }, 200);
            }
        }, 'a.tag');

        var wrap = this;
        $(wrap).append('<div id="tag_container"></div>');
        var tags = new Array();
        $('.tag', wrap).each(function () {
            tags.push($(this).attr('data-src'));
        });

        var height = wrap.height();
        var width = wrap.width();
        var rows = opts.row;
        var columns = opts.col;

        $(window).on('load', function () {
            Collage(tags, width, height, columns, rows);
        });
        $(window).on('resize', function () {
     
            var newWidth = wrap.width();
            if (newWidth != width) {
                width = newWidth;//for those kind of people that keep resizing their browser window, wups I'm guilty of that!    
                Collage(tags, width, height, columns, rows);
            }
        });
    
        function Collage(tags, width, height, columns, rows) {
            
            //determine bottom and right margin between images
            var margin = (Math.floor(width / (columns * 2)) / 10) - (Math.floor(width / (columns * 2)) / 10) / columns;

            //create grid array to store offsets
            this.grid = new Array(rows);
            for (var r = 0; r < this.grid.length; r++) {
                this.grid[r] = new Array(columns);
            }
            var img = 0;
            //determine the value in pixels of a singular row column and row unit
            var column_unit = Math.floor(width / columns);
            var row_unit = Math.floor(height / rows);
            $('#tag_container').empty();
            for (var r = 0; r < this.grid.length; r++) {
                for (var c = 0; c < this.grid[r].length; c++) {

                    //skip the block if its occupied
                    if (this.grid[r][c] !== 1) {

                        var src = tags[img++ % tags.length];
                        var el = new Image();
                        var tagname = src.substring(src.indexOf('[') + 1, src.indexOf(']'));
                        var columns = $(this).getColumns(this.grid, r, c);
                        var rows = $(this).getRows(this.grid, r, c);

                        //push the images x & y values
                        if (el.dataset !== undefined) {
                            $(el).css({
                                'left': c * column_unit + (margin / 1.5),
                                'top': r * row_unit + (margin / 1.5),
                                'width': column_unit * columns - margin,
                                'height': row_unit * rows - margin
                            });
                        }
                        //internet explorer wooppss
                        else {
                            $(el).css({
                                'left': c * column_unit + (margin / 2),
                                'top': r * row_unit + (margin / 2),
                                'width': column_unit * columns - margin,
                                'height': row_unit * rows - margin
                            });
                        }
                        if (src.indexOf('[') >= 0) {
                            src = src.substring(0, src.indexOf('['));
                        }
                        $(el).data('data-tag', tagname);
                        el.src = src;
                        el.onload = function () {
                            var c = $(document.createElement('a'));
                            $(c).attr('width', $(this).attr('width'));
                            $(c).attr('height', $(this).attr('height'));

                            var w = $(this).css('width');
                            var h = $(this).css('height');
                            var ar;
                            
                            if (this.naturalHeight !== undefined || this.naturalWidth !== undefined) {
                                ar = this.naturalWidth / this.naturalHeight;
                            }
                            else {
                                ar = this.width / this.height;
                            }
                  
                            if ($(this).height() > $(this).width()) {
                                w = parseFloat($(this).height()) * ar;
                                if (w < $(this).width()) {
                                    w = $(this).width();
                                    h = $(this).width() / ar;
                                }
                            }
                            else {
                                h = parseFloat($(this).width()) / ar;
                                if (h < $(this).height()) {
                                    h = $(this).height();
                                    w = $(this).height() * ar
                                }
                            }
                            $(c).addClass('tag');                     
                            $(c).css({
                                'overflow': 'hidden',
                                'position': 'absolute',
                                'left': $(this).css('left'),
                                'top': $(this).css('top'),
                                'max-height': $(this).css('height'),
                                'max-width': $(this).css('width')
                            });
                            $(this).css({
                                'width': w + 'px',
                                'height': h + 'px'
                            });
                            $(this).addClass('tag');
                            $(c).attr('href', 'search?q=' + $(this).data('data-tag'));
                            $(c).append(this);
                            $('#tag_container').append(c);
                            $(c).hide();
                            $(c).fadeIn(400);
                        }

                        for (var or = r; or < r + rows; or++) {
                            for (var oc = c; oc < c + columns; oc++) {
                                this.grid[or][oc] = 1;
                            }
                        }         
                    }
                }
            }
        } 
    }
})(jQuery);

(function ($) {
    $.fn.getColumns = function (grid, row, column) {
        var columns = grid[row].length;
        if (column + 1 < columns && !grid[row][column + 1] && Math.random() * 100 < 30) {
            return 2;
        }
        else {
            return 1;
        }
    }
})(jQuery);

(function ($) {
    $.fn.getRows = function (grid, row, column) {
        var rows = grid.length;
        if (row + 1 < rows && !grid[row + 1][column] && Math.random() * 100 < 30) {
            return 2;
        }
        else {
            return 1;
        }
    }
})(jQuery);
