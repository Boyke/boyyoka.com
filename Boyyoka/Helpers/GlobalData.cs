﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Boyyoka.Models;

namespace Boyyoka.Helpers
{
    public static class GlobalData
    {
        public static int wallpaperCount;
        public static int featuredIndexId;
        public static int randomIndex;
        public static List<Wallpaper> featuredWallpapers;
        //basically the most famous wallpaper of certain tags will be collected and shown
        public static List<Tag> hotTags;
        public static List<User> newUsers;

        public static void init()
        {
            setFeatured();
            setTags();
            setFeaturedIndexId();
            setRandomIndex();
            setWallpaperCount();
            setNewUsers();
        }
        //Sets the featured wallpapers on the homepage
        private static void setFeatured()
        {
            using (var context = new BoyyoyoContext())
            {
                featuredWallpapers = context.Wallpapers
                    .Where(w => w.Date <= DbFunctions.AddDays(w.Date, 5))
                    .OrderByDescending(w => w.Date)
                    .Take(20).ToList();
            }
        }

        //Sets the hottest tags of the week on the homepage
        private static void setTags()
        {
            using (var context = new BoyyoyoContext())
            {
                hotTags = context.Tags
                    .Where(t => t.Date <= DbFunctions.AddDays(t.Date, 5) && t.Wallpapers.Count > 2)
                    .Include(t => t.Wallpapers)
                    .OrderByDescending(t => t.Tagged)
                    .Take(18).ToList();
            }
        }

        public static void setNewUsers()
        { 
            using(var context = new BoyyoyoContext())
            {
                newUsers = context.Users
                    .OrderByDescending(u => u.JoinDate)
                    .Take(11).ToList();
            }
        }

        //This method only gets the wallpaper count from the BoyyokaGlobal table, which has no relation to the context or any other entities.
        private static void setWallpaperCount() 
        {       
            string connectionString =  ConfigurationManager.ConnectionStrings["BoyyoyoContext"].ConnectionString;   
            using(var conn = new SqlConnection(connectionString))
            {
                using(var cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = "SELECT WallpaperCount FROM BoyyokaGlobal";
                    using(var reader = cmd.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            wallpaperCount = reader.GetInt32(0);
                        }
                    }
                }               
            }     
        }

        //This method only modifies the wallpaper count from the BoyyokaGlobal table, which has no relation to the context or any other entities.
        public static int incrementWallpaperCount()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["BoyyoyoContext"].ConnectionString;
            using (var conn = new SqlConnection(connectionString))
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "UPDATE BoyyokaGlobal SET WallpaperCount = WallpaperCount + 1";
                    conn.Open();
                    cmd.Prepare();
                    cmd.ExecuteNonQuery();
                    wallpaperCount++;
                }
            }
            return wallpaperCount;
        }

        private static void setFeaturedIndexId()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["BoyyoyoContext"].ConnectionString;
            using (var conn = new SqlConnection(connectionString))
            {
                using (var cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = "SELECT FeaturedIndexId FROM BoyyokaGlobal";
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            featuredIndexId = reader.GetInt32(0);
                        }
                    }
                }
            }
        }

        private static void setRandomIndex()
        {
            randomIndex = new Random().Next(0, 5);
        }
    }
}