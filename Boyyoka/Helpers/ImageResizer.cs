﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace Boyyoka.Helpers
{
    /*http://codepedia.info/how-to-resize-image-while-uploading-in-asp-net-using-c/*/
    public static class ImageResizer
    {
        public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioY = (double)maxHeight / image.Height;
            var ratioX = (double)maxWidth / image.Width;
            int newWidth = 0;
            int newHeight = 0;
            //var posX = Convert.ToInt32((maxWidth - (image.Width * ratioY)) / 2);
            //var posY = Convert.ToInt32((maxHeight - (image.Height * ratioY)) / 2);
            if (ratioY > ratioX)
            {
                newWidth = ((int)(image.Width * ratioY));
                newHeight = (int)(image.Height * ratioY);
            }
            else
            {
                newWidth = ((int)(image.Width * ratioX));
                newHeight = (int)(image.Height * ratioX);
            }

            var newImage = new Bitmap(newWidth, newHeight);

            using (var g = Graphics.FromImage(newImage))
            {
                g.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            return newImage;
        }

        /*http://stackoverflow.com/questions/10323633/resize-image-in-c-sharp-with-aspect-ratio-and-crop-central-image-so-there-are-no*/
        public static Image CreateThumbnail(Image image, int maxWidth, int maxHeight, bool needToFill)
        {
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            int sourceX = 0;
            int sourceY = 0;
            double destX = 0;
            double destY = 0;

            double nScale = 0;
            double nScaleW = 0;
            double nScaleH = 0;

            nScaleW = ((double)maxWidth / (double)sourceWidth);
            nScaleH = ((double)maxHeight / (double)sourceHeight);
            if (!needToFill)
            {
                nScale = Math.Min(nScaleH, nScaleW);
            }
            else
            {
                nScale = Math.Max(nScaleH, nScaleW);
                destY = (maxHeight - sourceHeight * nScale) / 2;
                destX = (maxWidth - sourceWidth * nScale) / 2;
            }

            if (nScale > 1)
                nScale = 1;

            int destWidth = (int)Math.Round(sourceWidth * nScale);
            int destHeight = (int)Math.Round(sourceHeight * nScale);

            Bitmap bmPhoto = null;
            try
            {
                bmPhoto = new Bitmap(destWidth + (int)Math.Round(2 * destX), destHeight + (int)Math.Round(2 * destY));
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("destWidth:{0}, destX:{1}, destHeight:{2}, desxtY:{3}, Width:{4}, Height:{5}",
                    destWidth, destX, destHeight, destY, maxWidth, maxHeight), ex);
            }
            using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
            {
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.CompositingQuality = CompositingQuality.HighQuality;
                grPhoto.SmoothingMode = SmoothingMode.HighQuality;

                Rectangle to = new Rectangle((int)Math.Round(destX), (int)Math.Round(destY), destWidth, destHeight);
                Rectangle from = new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
                grPhoto.DrawImage(image, to, from, GraphicsUnit.Pixel);

                return bmPhoto;
            }
        }          
    }
}