﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.SessionState;
using Boyyoka.Helpers;
using Boyyoka.ViewModels;
using Boyyoka.Models;
using Boyyoka.Models.Interfaces;
using Boyyoka.Interfaces;
using System.Drawing;
using System.Drawing.Imaging;


namespace Boyyoka.Services
{
    public class UserService : IUserService
    {
        public bool SigninUser(SigninViewModel model)
        {
            using(var context = new BoyyoyoContext())
            {
                var user = context.Users
                           .FirstOrDefault(
                               u => u.Username == model.Username
                          );
                if (user != null && BCrypt.CheckPassword(model.Password, user.Password))
                {
                    SessionManager.SessionManager.setString("uname", user.Username);
                    return true;
                }
                else 
                {
                    return false;
                }
            }
        }

        public bool SignoutUser()
        {
            if (!String.IsNullOrEmpty(SessionManager.SessionManager.getString("uname")))
            {
                SessionManager.SessionManager.Session.Abandon();
                return true;
            }
            else 
            {
                return false;
            }
        }

        public bool RegisterNewUser(RegisterViewModel model)
        {    
            using (var context = new BoyyoyoContext())
            {
                var user = new User();
                user.Username = model.Username;
                user.Email = model.Email;
                user.Password = BCrypt.HashPassword(model.Password, BCrypt.GenerateSalt(12));
                user.JoinDate = DateTime.UtcNow;
                context.Users.Add(user);
                return context.SaveChanges() > 0 ? true : false;             
            }   
        }

        public void UploadWallpaper(HttpRequestBase Request)
        {
            List<Wallpaper> wallpapers = new List<Wallpaper>();
            User user = null;

            using (var context = new BoyyoyoContext())
            {
                var userName = SessionManager.SessionManager.getString("uname");
                user = context.Users.FirstOrDefault(u => u.Username.Equals(userName.ToString()));

                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    if (file != null && file.ContentLength > 0)
                    {
                        if (file.ContentType == "image/jpeg"
                            || file.ContentType == "image/png"
                            || file.ContentType == "image/gif")
                        {
                            char[] charSplits = new char[] { ',' };
                            List<Tag> tags = new List<Tag>();

                            //gets the actual filename
                            var _fileName = Path.GetFileName(file.FileName);
                            var tagsRequest = Request["TAGS" + _fileName];
                            var tagNames = tagsRequest.Split(charSplits, StringSplitOptions.RemoveEmptyEntries).ToArray();

                            if (tagNames.Length > 0)
                            {
                                foreach (string tagName in tagNames)
                                {
                                    tags.Add(DuplicateHelper.GetTag(tagName, context, user));
                                }
                            }

                            var resolution = Request[_fileName];
                            int height = int.Parse(resolution.Substring(resolution.IndexOf("x") + 1));
                            int width = int.Parse(resolution.Substring(0, resolution.IndexOf("x")));
                            int wallpaperNumber = GlobalData.incrementWallpaperCount();

                            var wallpaperType = Path.GetExtension(_fileName);
                            var absolutePath = Path.Combine(HostingEnvironment.MapPath("~/Wallpapers/"), "wallpaper-" + wallpaperNumber + wallpaperType);
                            var relativePath = absolutePath.Replace(Request.ServerVariables["APPL_PHYSICAL_PATH"], String.Empty);

                            //for thumbnail
                            var thumbAbsolutePath = Path.Combine(HostingEnvironment.MapPath("~/Thumbnails/"), "thumb-" + wallpaperNumber + ".jpeg");
                            var thumbRelativePath = thumbAbsolutePath.Replace(Request.ServerVariables["APPL_PHYSICAL_PATH"], String.Empty);
                            Bitmap image = new Bitmap(file.InputStream);
                            Image thumbnail = ImageResizer.CreateThumbnail(image, 250, 250, true);
                            
                            var wallpaper = new Wallpaper()
                            {
                                User = user,
                                Resolution = resolution,
                                Path = relativePath,
                                Thumbnail = thumbRelativePath,
                                Views = 0,
                                Type = wallpaperType,
                                Height = height,
                                Width = width,
                                Date = DateTime.Today,
                                Tags = tags
                            };

                            wallpapers.Add(wallpaper);
                            thumbnail.Save(thumbAbsolutePath, ImageFormat.Jpeg);
                            file.SaveAs(absolutePath);
                        }
                    }                
                }

                wallpapers.ForEach(w => context.Wallpapers.Add(w));
                context.Entry(user).State = System.Data.Entity.EntityState.Unchanged;
                context.SaveChanges();
            }        
        }
    }
}