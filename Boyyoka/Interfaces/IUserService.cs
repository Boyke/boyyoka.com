﻿using Boyyoka.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Boyyoka.ViewModels;
using System.Web;

namespace Boyyoka.Interfaces
{
    public interface IUserService : IService
    {
        bool SignoutUser();
        bool SigninUser(SigninViewModel model);
        bool RegisterNewUser(RegisterViewModel model);
        void UploadWallpaper(HttpRequestBase Request);
    }
}
