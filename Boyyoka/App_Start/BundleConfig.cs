﻿using System.Web;
using System.Web.Optimization;

namespace Boyyoka.App_Start
{
    public class BundleConfig
    {

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Content/Scripts/jquery.min.js",
                        "~/Content/Scripts/jquery.jscroll.min.js",
                        "~/Content/Scripts/jquery.kinetic.min.js"               
            ));

            bundles.Add(new ScriptBundle("~/bundles/homepage/scripts").Include(
                        "~/Content/Scripts/jquery.easing.js",
                        "~/Content/Scripts/jquery.tagCollection.js",
                        "~/Content/Scripts/jquery.kaslider.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                        "~/Content/Stylesheets/stylesheet.css"
            ));
            BundleTable.EnableOptimizations = true;
        }
    }
}