﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Boyyoka
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.LowercaseUrls = true;

            routes.MapRoute(
                name: "Upload",
                url: "Upload",
                defaults: new { controller = "User", action = "Upload", id = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "LoadWallpapers",
               url: "Load/{page}",
               defaults: new { controller = "Search", action = "Load", page = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "New",
                url: "New",
                defaults: new { controller = "Search", action = "New" }
            );
            routes.MapRoute(
                name: "Search",
                url: "Search/{searchQuery}",
                defaults: new { controller = "Search", action = "Search", searchQuery = UrlParameter.Optional }
            );
            routes.MapRoute(
                    name: "Signin",
                    url: "User/Signin/{*url}",
                    defaults: new { controller = "User", action = "Signin", url = UrlParameter.Optional }
            );
            routes.MapRoute(
                 name: "Signout",
                 url: "User/Signout/{*url}",
                 defaults: new { controller = "User", action = "Signout", url = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "Register",
               url: "User/Register",
               defaults: new { controller = "User", action = "Register", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                 name: "Wallpaper",
                 url: "Wallpaper/{id}",
                 defaults: new { controller = "Wallpaper", action = "Wallpaper", id = "" }
            );
            routes.MapRoute(
               name: "Default",
               url: "{controller}",
               defaults: new { controller = "Home", action = "Index"}
           );
            /*
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
             
            routes.MapRoute(
                name: "Profile",
                url: "User/Profile",
                defaults: new { controller = "User", action = "Profile", id = UrlParameter.Optional }
           ); 
             
            */
        }
    }
}
