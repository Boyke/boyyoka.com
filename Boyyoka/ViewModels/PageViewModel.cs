﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Boyyoka.ViewModels
{
    //For general page information
    public class PageViewModel
    {
        public int RangeStart { get; set; }
        public int RangeEnd { get; set; }
        public int Pagesize { get; set; }
    }
}