﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Boyyoka.ViewModels
{
    public class SigninViewModel
    {
        [Required(ErrorMessage = "Please provide a username", AllowEmptyStrings = false)]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please provide a password", AllowEmptyStrings = false)]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}