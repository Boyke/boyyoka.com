﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Boyyoka.ViewModels
{
    public class TagViewModel
    {
        public string TagName { get; set; }
        public int Tagged { get; set; }
        public DateTime Date { get; set; }
        public string WallpaperPath { get; set; }
    }
}