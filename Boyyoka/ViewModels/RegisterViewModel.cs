﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Boyyoka.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage="Please provide a username", AllowEmptyStrings=false)]
        public string Username { get; set; }

        [Required(ErrorMessage="Please provide an email address", AllowEmptyStrings=false)]
        [RegularExpression(@"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,3})$", ErrorMessage="Please provide a valid email address")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage="Please provide a password", AllowEmptyStrings=false)]
        public string Password { get; set; }
        
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        public string ReturnUrl { get; set; }
    }
}