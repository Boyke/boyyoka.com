﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Boyyoka.Models;
using Boyyoka.Controllers;
using System.Web.Http.Controllers;

namespace Boyyoka.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class TrackFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            int wallpaperId = Convert.ToInt32(filterContext.ActionParameters.Values.First());

            if (SessionManager.SessionManager.Session["ViewedWallpapers"] != null)
            {
                List<int> ViewedWallpaperIds = 
                    (List<int>)SessionManager.SessionManager.Session["ViewedWallpapers"];

                if (!ViewedWallpaperIds.Exists(id => id == wallpaperId))
                {
                    ViewedWallpaperIds.Add(wallpaperId);
                    using(var context = new BoyyoyoContext())
                    {
                        var _wallpaperId = "wallpaper-" + wallpaperId;
                        var wallpaper = context.Wallpapers.Where(w => w.Path.Contains(_wallpaperId) == true).FirstOrDefault();
                        wallpaper.Views++;
                        context.SaveChanges();
                    }
                }              
            }
            else
            {
                List<int> ViewedWallpaperIds = new List<int>();
                ViewedWallpaperIds.Add(wallpaperId);
                SessionManager.SessionManager.Set<List<int>>("ViewedWallpapers", ViewedWallpaperIds);
            
            }   
        }     
    }
}