﻿using Boyyoka.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Boyyoka.Models.Interfaces;
using System.Text.RegularExpressions;
using Boyyoka.Models;

namespace Boyyoka.Controllers
{
    public class SearchController : Controller
    {
        private ISearchService _searchService;

        public SearchController(ISearchService searchService)
        {
            _searchService = searchService;
        }
      
        //
        // POST: /Search/
        public ActionResult Search(string q)
        {
            var wrapper = new ViewModelWrapper();
            wrapper.PageViewModel = new PageViewModel();
            wrapper.WallpapersViewModel = new List<WallpaperViewModel>();
            wrapper.SearchViewModel = new SearchViewModel();
            wrapper.SearchViewModel.q = q;

            var wallpapers = _searchService.LoadWallpapers("search", q).ToList();

            if (wallpapers != null || wallpapers.Count() == 0)
            {
                foreach (var wallpaper in wallpapers)
                {
                    var WallpaperViewModel = new WallpaperViewModel();
                    ModelBinder(WallpaperViewModel, wallpaper);
                    wrapper.WallpapersViewModel.Add(WallpaperViewModel);
                }

                wrapper.PageViewModel.Pagesize = 30;
                wrapper.PageViewModel.RangeStart = 0;
                wrapper.PageViewModel.RangeEnd = wrapper.PageViewModel.RangeStart + wallpapers.Count;
                ViewBag.Page = 1;
                SessionManager.SessionManager.Set("Caller", "search");
                return View(wrapper);
            }
            else
            {
                return View(ViewBag.Message = "Sry, no results found");
            }
        }

        //
        // GET: /New wallpapers/
        public ActionResult New()
        {
            var wrapper = new ViewModelWrapper();
            wrapper.PageViewModel = new PageViewModel();
            wrapper.WallpapersViewModel = new List<WallpaperViewModel>();
            wrapper.SearchViewModel = new SearchViewModel();//prevent null error when you search from the user bar
            var wallpapers = _searchService.LoadWallpapers("new").ToList();
            if (wallpapers != null || wallpapers.Count() == 0)
            {
                foreach (var wallpaper in wallpapers)
                {
                    var WallpaperViewModel = new WallpaperViewModel();
                    ModelBinder(WallpaperViewModel, wallpaper);
                    wrapper.WallpapersViewModel.Add(WallpaperViewModel);
                }
                wrapper.PageViewModel.Pagesize = 30;
                wrapper.PageViewModel.RangeStart = 0;
                wrapper.PageViewModel.RangeEnd = wrapper.PageViewModel.RangeStart + wallpapers.Count;
                ViewBag.Page = 1;                
                SessionManager.SessionManager.Set("Caller", "new");
                return View(wrapper);
            }
            else
            {
                return View(ViewBag.Message = "Sry, no results found");
            }
        }
      
        //
        // Load more new wallpapers/     
        public ActionResult Load(string q = "", int page = 0)
        {
            var wrapper = new ViewModelWrapper();
            wrapper.PageViewModel = new PageViewModel();
            wrapper.WallpapersViewModel = new List<WallpaperViewModel>();
            wrapper.SearchViewModel = new SearchViewModel();
            wrapper.SearchViewModel.q = q;

            string caller = SessionManager.SessionManager.getString("Caller");
            var wallpapers = _searchService.LoadWallpapers(caller, q, page).ToList();
            if (wallpapers.Count == 0)
                return new EmptyResult();

            foreach (var wallpaper in wallpapers)
            {
                var WallpaperViewModel = new WallpaperViewModel();
                ModelBinder(WallpaperViewModel, wallpaper);
                wrapper.WallpapersViewModel.Add(WallpaperViewModel);
            }
            wrapper.PageViewModel.Pagesize = 30;
            wrapper.PageViewModel.RangeStart = page * wrapper.PageViewModel.Pagesize;
            wrapper.PageViewModel.RangeEnd = wrapper.PageViewModel.RangeStart + wallpapers.Count;
            ViewBag.Page = page + 1;       
            return PartialView("_MoreWallpapers", wrapper);
        }

        //mapping properties from model to viewmodel
        private void ModelBinder(WallpaperViewModel WallpaperViewModel, Wallpaper wallpaper)
        {
            WallpaperViewModel.WallpaperID = wallpaper.WallpaperID;
            WallpaperViewModel.ThumbnailPath = wallpaper.Thumbnail;
            WallpaperViewModel.ImagePath = wallpaper.Path;
            WallpaperViewModel.ImageId = Regex.Replace(wallpaper.Path, @"[^\d]", "");
            WallpaperViewModel.Date = wallpaper.Date;
            //ViewModelWrapper.WallpaperViewModel.Uploader = wallpaper.User.Username;
            WallpaperViewModel.Purity = wallpaper.Purity;
            WallpaperViewModel.Resolution = wallpaper.Resolution;
            WallpaperViewModel.Type = wallpaper.Type;
            WallpaperViewModel.Views = wallpaper.Views;
            WallpaperViewModel.isFeatured = false;
            WallpaperViewModel.Tags = wallpaper.Tags;
        }
	}
}