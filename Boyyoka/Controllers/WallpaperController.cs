﻿using Boyyoka.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Boyyoka.Models;
using System.Text.RegularExpressions;
using System.Text;
using Boyyoka.Filters;

namespace Boyyoka.Controllers
{
    public class WallpaperController : Controller
    {
        //
        // GET: /Wallpaper/
        [TrackFilter]
        public ActionResult Wallpaper(string id)
        {      
            var wrapper = new ViewModelWrapper();
            wrapper.WallpaperViewModel = new WallpaperViewModel();
            var wallpaperId = "wallpaper-" + id + ".";//dot indicates end of string, as the type of the wallpaper doesn't matter in this context
            using(var db = new BoyyoyoContext())
            {
                var wallpaper = db.Wallpapers.Where(w => w.Path.Contains(wallpaperId) == true).FirstOrDefault();
                if (wallpaper != null)
                {
                    wallpaper.User = db.Users.Find(wallpaper.UserID);
                    wrapper.WallpaperViewModel.WallpaperID = wallpaper.WallpaperID;
                    wrapper.WallpaperViewModel.ImageId = id;
                    wrapper.WallpaperViewModel.Date = wallpaper.Date;
                    wrapper.WallpaperViewModel.Uploader = wallpaper.User.Username;
                    wrapper.WallpaperViewModel.Purity = wallpaper.Purity;
                    wrapper.WallpaperViewModel.Resolution = wallpaper.Resolution;
                    wrapper.WallpaperViewModel.ImagePath = wallpaper.Path;
                    wrapper.WallpaperViewModel.Type = wallpaper.Type;
                    wrapper.WallpaperViewModel.Views = wallpaper.Views;
                    wrapper.WallpaperViewModel.Favorites = wallpaper.Users.Count();
                    wrapper.WallpaperViewModel.Tags = wallpaper.Tags;
                    //wallpaper.Views++;

                    //db.SaveChanges();   
                    return View(wrapper);
                }
            }
            return View(ViewBag.Message ="Sry, no results found");
        }
	}
}