﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Boyyoka.ViewModels;
using Boyyoka.Models;
using Boyyoka.Helpers;

namespace Boyyoka.Controllers
{

    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            DateTime now = DateTime.UtcNow;
            var wrapper = new ViewModelWrapper();
            wrapper.WallpapersViewModel = new List<WallpaperViewModel>();
            wrapper.TagsViewModel = new List<TagViewModel>();
            wrapper.NewUsers = new List<UserViewModel>();
            wrapper.SearchViewModel = new SearchViewModel();

            using(var db = new BoyyoyoContext())
            {
                var featuredWallpapers = GlobalData.featuredWallpapers;
                var hotTags = GlobalData.hotTags;
                var newUsers = GlobalData.newUsers;

                foreach(var wallpaper in featuredWallpapers)
                {
                    wrapper.WallpaperViewModel = new WallpaperViewModel();
                    wrapper.WallpaperViewModel.WallpaperID = wallpaper.WallpaperID;
                    wrapper.WallpaperViewModel.Date = wallpaper.Date;
                    //ViewModelWrapper.WallpaperViewModel.Uploader = wallpaper.User.Username;
                    wrapper.WallpaperViewModel.Purity = wallpaper.Purity;
                    wrapper.WallpaperViewModel.Resolution = wallpaper.Resolution;
                    wrapper.WallpaperViewModel.ImagePath = wallpaper.Path;
                    wrapper.WallpaperViewModel.ThumbnailPath = wallpaper.Thumbnail;
                    wrapper.WallpaperViewModel.Type = wallpaper.Type;
                    wrapper.WallpaperViewModel.Views = wallpaper.Views;
                    wrapper.WallpaperViewModel.isFeatured = false;                
                    wrapper.WallpapersViewModel.Add(wrapper.WallpaperViewModel);
                }

                foreach(var tag in hotTags)
                {
                    TagViewModel tagViewModel = new TagViewModel();
                    tagViewModel.TagName = tag.TagName;
                    tagViewModel.Tagged = tag.Tagged;
                    tagViewModel.Date = tag.Date;
                    if (tag.Wallpapers.Count > GlobalData.randomIndex
                        &&tag.Wallpapers.ElementAt(GlobalData.randomIndex) != null)
                    {
                        tagViewModel.WallpaperPath = tag.Wallpapers.ElementAt(GlobalData.randomIndex).Path;
                    }
                    else 
                    {
                        tagViewModel.WallpaperPath = tag.Wallpapers.First().Path;
                    }
                    wrapper.TagsViewModel.Add(tagViewModel);
                }

                foreach(var user in newUsers)
                {
                    string _JoinDate = null;
                    if (user.JoinDate > now.AddMinutes(-60) && user.JoinDate <= now)
                    {
                        double minutes = (now - user.JoinDate).TotalMinutes;
                        _JoinDate = Math.Round(minutes).ToString() + " minutes";
                    }
                    else if (user.JoinDate > now.AddHours(-24) && user.JoinDate <= now)
                    {
                        double hours = (now - user.JoinDate).TotalHours;
                        _JoinDate = Math.Round(hours).ToString() + " hours";
                    }
                    else 
                    {
                        double days = (now - user.JoinDate).TotalDays;
                        _JoinDate = Math.Round(days).ToString() + " days";
                    }
                    UserViewModel userViewModel = new UserViewModel();
                    userViewModel.UserName = user.Username;     
                    userViewModel.JoinDate = _JoinDate;
                    wrapper.NewUsers.Add(userViewModel);
                }
            }       

            if(wrapper.WallpapersViewModel.Count > 0)
            {
                var featuredWallpaper = wrapper.WallpapersViewModel.ElementAt(GlobalData.featuredIndexId);
                featuredWallpaper.isFeatured = true;
            }

            return View(wrapper);
        }
	}
}