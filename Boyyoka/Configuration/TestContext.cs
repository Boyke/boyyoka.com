namespace Boyyoka
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TestContext : DbContext
    {
        public TestContext()
            : base("name=TestContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<TestUser> TestUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TestUser>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<TestUser>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<TestUser>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<TestUser>()
                .Property(e => e.Avatar)
                .IsUnicode(false);
        }
    }
}
